const express = require("express");
var app = express();
const bodyParser = require("body-parser");
const AccountModel = require("./model/account");
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.post("/login", (req, res, next) => {
  let username = req.body.usename;
  let password = req.body.password;
  AccountModel.findOne({username: username, password: password})
    .then((data) => {
      if (data) {
        res.json("login thanh cong");
      } else {
        res.status(300).json("account ko dung");
      }
    })
    .catch((err) => {
      res.status(500).json("co loi ben server");
    });
});
app.post("/register", (req, res, next) => {
  let username = req.body.username;
  let password = req.body.password;
  AccountModel.findOne({
    username: username,
  }).then((data) => {
    if (data) {
      res.json("tai khoan da ton tai");
    } else {
      AccountModel.create({username: username, password: password})
        .then((data) => {
          res.json("tao tai khoan thanh cong");
        })
        .catch((err) => {
          res.status(500).json("tao tai khoan that bai");
        });
    }
  });
});
app.get("/", (req, res, next) => {
  res.json("HOME");
});
app.listen(3000, () => {
  console.log("run");
});
